#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "option_parser.h"

class Encoder {
 public:
  virtual ~Encoder() = 0;
  virtual std::string Encode(std::vector<uint8_t>) = 0;
  virtual ConversionBase GetType() = 0;
};