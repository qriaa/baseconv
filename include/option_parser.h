#pragma once

#include <string>
#include <unordered_map>

enum ConversionBase { ASCII, BASE64 };

struct Options {
  ConversionBase input_base;
  ConversionBase output_base;
  std::string working_directory;
  std::string input_filepath;
  std::string output_filepath;
};

class OptionParser {
 private:
  const char* getopt_string_ = "i:o:f:";
  const std::unordered_map<std::string, ConversionBase>
      base_translation_table_ = {{"ascii", ASCII}, {"base64", BASE64}};
  Options parsed_options_;

  ConversionBase TranslateBase(std::string base_str);

 public:
  void Parse(int argc, char* argv[]);
  Options GetOptions();
};