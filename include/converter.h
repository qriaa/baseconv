#pragma once

#include <memory>

#include "decoder.h"
#include "encoder.h"
#include "option_parser.h"

class Converter {
 private:
  Options options_;
  std::unique_ptr<Encoder> encoder_;
  std::unique_ptr<Decoder> decoder_;

  void EncoderDecoderInit();

 public:
  Converter(Options);
  void Convert();
};