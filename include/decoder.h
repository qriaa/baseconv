#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "option_parser.h"

class Decoder {
 public:
  virtual ~Decoder() = 0;
  virtual std::vector<uint8_t> Decode(std::string) = 0;
  virtual ConversionBase GetType() = 0;
};