#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "../decoder.h"
#include "../option_parser.h"

class AsciiDecoder : public Decoder {
 private:
  const ConversionBase conversion_base_ = ASCII;

 public:
  std::vector<uint8_t> Decode(std::string) override final;
  ConversionBase GetType() override final;
};