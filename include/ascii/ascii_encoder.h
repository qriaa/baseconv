#pragma once

#include <cstdint>
#include <string>
#include <vector>

#include "../encoder.h"
#include "../option_parser.h"

class AsciiEncoder : public Encoder {
 private:
  const ConversionBase conversion_base_ = ASCII;

 public:
  std::string Encode(std::vector<uint8_t>) override final;
  ConversionBase GetType() override final;
};