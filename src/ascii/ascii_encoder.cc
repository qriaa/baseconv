#include "ascii/ascii_encoder.h"

std::string AsciiEncoder::Encode(std::vector<uint8_t> buffer) {
  std::string result = std::string();
  for (uint8_t byte : buffer) {
    result.append(1, static_cast<char>(byte));
  }
  return result;
}

ConversionBase AsciiEncoder::GetType() { return conversion_base_; }