#include "ascii/ascii_decoder.h"

std::vector<uint8_t> AsciiDecoder::Decode(std::string buffer) {
  std::vector<uint8_t> result = std::vector<uint8_t>();
  for (char character : buffer) {
    result.push_back(static_cast<uint8_t>(character));
  }
  return result;
}

ConversionBase AsciiDecoder::GetType() { return conversion_base_; }