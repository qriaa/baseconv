#include "converter.h"

#include <fstream>
#include <iostream>

#include "ascii/ascii_decoder.h"
#include "ascii/ascii_encoder.h"

Converter::Converter(Options options) {
  this->options_ = options;
  EncoderDecoderInit();
}

void Converter::EncoderDecoderInit() {
  // Init decoder
  switch (options_.input_base) {
    case ASCII:
      decoder_ = std::make_unique<AsciiDecoder>();
      break;
    case BASE64:
      // TODO: add base64
      break;
  }

  // Init encoder
  switch (options_.output_base) {
    case ASCII:
      encoder_ = std::make_unique<AsciiEncoder>();
      break;
    case BASE64:
      // TODO: add base64
      break;
  }
}

void Converter::Convert() {
  std::ifstream ifile = std::ifstream();
  std::ofstream ofile = std::ofstream();

  // open ifile if specified file
  if (options_.input_filepath != "") {
    ifile.open(options_.input_filepath, std::ios::in);
    if (ifile.fail()) {
      std::cout << "Input file not found. Terminating.\n";
      exit(1);
    }
  }

  // open ofile if specified file
  if (options_.output_filepath != "") {
    ofile.open(options_.output_filepath, std::ios::out);
    if (ofile.fail()) {
      std::cout << "Opening output file failed. Terminating.\n";
      exit(1);
    }
  }

  std::istream& input = ifile.is_open() ? ifile : std::cin;
  std::ostream& output = ofile.is_open() ? ofile : std::cout;
}