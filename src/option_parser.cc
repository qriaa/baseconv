#include "option_parser.h"

#include <unistd.h>

#include <filesystem>
#include <iostream>
#include <stdexcept>

ConversionBase OptionParser::TranslateBase(std::string base_str) {
  try {
    return base_translation_table_.at(base_str);
  } catch (std::out_of_range e) {
    std::cout << "No such base available: " + base_str + "\n";
    exit(1);
  }
}

void OptionParser::Parse(int argc, char* argv[]) {
  Options new_options = Options();

  new_options.input_filepath = "";
  new_options.output_filepath = "";
  new_options.working_directory = std::filesystem::current_path();

  int opt;
  while ((opt = getopt(argc, argv, this->getopt_string_)) != -1) {
    switch (opt) {
      case 'i':
        new_options.input_base = TranslateBase(optarg);
        break;
      case 'o':
        new_options.output_base = TranslateBase(optarg);
        break;
      case 'f':
        new_options.input_filepath = optarg;
        break;
      case '?':  // non-existent option or missing option argument
        exit(1);
        break;
    }
  }

  if (argc > optind) {
    new_options.output_filepath = argv[optind];
  } else {
    new_options.output_filepath = "";
  }

  parsed_options_ = new_options;
}

Options OptionParser::GetOptions() { return this->parsed_options_; }
