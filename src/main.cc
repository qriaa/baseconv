#include <iostream>

#include "option_parser.h"

int main(int argc, char* argv[]) {
  OptionParser parser = OptionParser();

  parser.Parse(argc, argv);

  Options options = parser.GetOptions();

  std::cout << options.input_base << std::endl;
  std::cout << options.output_base << std::endl;
  std::cout << options.working_directory << std::endl;
  std::cout << options.input_filepath << std::endl;
  std::cout << options.output_filepath << std::endl;

  return 0;
}